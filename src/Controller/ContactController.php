<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Repository\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{

    // FORMULAIRE DE CONTACT
    #[Route('/contact', name: 'app_contact')]
    public function index(Request $request, EntityManagerInterface $em): Response
    {
        //Création du contact
        $contact = new contact();

        // On gère le formulaire de contact
        $form = $this->createFormBuilder($contact)
            ->add('name')
            ->add('email')
            ->add('content')
            ->add('submit', SubmitType::class, ['label' => "Envoyer"])
            ->getForm();

        // Traitement du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($contact);
            $em->flush();
            //après la validation, redirige sur la page d'accueil
            return $this->redirectToRoute('index');
        }


        return $this->render('contact/index.html.twig', [
            'createForm' => $form->createView()
        ]);
    }
}
