<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use ContainerGbi6pyx\getFosCkEditor_Form_TypeService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\TextEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ArticleCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return Article::class;
    }
    // CONFIGURATION CRUD
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Articles')
            ->setEntityLabelInSingular('Article')
            ->setPageTitle("index", " Blog Manga - Adminsitration des articles");
        // ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }
    // CONFIGURATION DES CHAMPS QUI COMPOSENT UN ARTICLE
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('title'),
            TextField::new('naneManga'),
            ImageField::new('image', FileType::class)->setUploadDir('public/image/'),
            TextField::new('content'),
            // TextField::new('author'),
            DateField::new('createdAt')->hideOnForm(),
            AssociationField::new('author'),

        ];
    }
    //  CREER UN ARTICLE 
    public function createEntity(string $entityFqcn)
    {
        $article = new Article();
        $article->setCreatedAT(new \DateTime());
        $article->setAuthor($this->getUser());
        return $article;
    }
}
