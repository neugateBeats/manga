<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{

     // AFFICHER LA PAGE D'ACCUEIL
     #[Route('/', name: 'main')]
     public function index(): Response
     {
          return $this->render('main/index.html.twig');
     }

     // AFFICHER TOUS LES ARTICLES 
     #[Route("/", name: "main", methods: "GET")]
     public function main(ArticleRepository $doctrine): Response
     {
          return $this->render(
               'main/index.html.twig',

               [
                    'articles' => $doctrine->findAll(),
               ]
          );
     }
}
