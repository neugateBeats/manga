<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use DateTime as GlobalDateTime;
// use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request as BrowserKitRequest;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\File;

class ArticleController extends AbstractController
{
    // AFFICHER TOUS LES ARTICLES 
    #[Route("/article", name: "app_article", methods: "GET")]
    public function index(ArticleRepository $doctrine): Response
    {
        return $this->render(
            'article/index.html.twig',

            [
                // récupérer tous les articles
                'articles' => $doctrine->findAll(),
            ]
        );
    }

    #[Route("article/view", name: 'app_view')]
    public function display(): Response
    {
        return $this->render('article/view.html.twig');
    }

    // AFFICHER UN ARTICLE 
    #[Route("article/view/{id}", name: "app_view", methods: "GET")]
    public function view(ArticleRepository $doctrine, int $id, Request $request, CommentRepository $doc,): Response
    {

        //PARTIE COMMENTAIRES

        // On crée le commantaire
        $comment = new Comment;

        // On génère le formulaire
        $commentForm = $this->createForm(CommentType::class, $comment);
        $commentForm->handleRequest($request);

        //traitement du formulaire
        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $comment->setCreatedAt(new \DateTime());
            // $comment->setArticles($article);

            $doc->persist($comment);
            $doc->flush();

            $this->addFlash('message', 'votre commentaire a bien été envoyé');
            return $this->redirectToRoute('app_view');
        }

        return $this->render(
            'article/view.html.twig',
            [
                // récuperer un article
                "article" => $doctrine->find($id),
                'form' => $commentForm->createView(),
                'commentForm' => $commentForm->createView(),

            ]
        );
    }
}
